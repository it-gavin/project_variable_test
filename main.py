import subprocess
import gitlab
import os

from config import get_settings

settings = get_settings()

def main():
    gl = gitlab.Gitlab(settings.URL, settings.TOKEN)
    # print(gl)
    project = gl.projects.get(settings.REPO)
    # projects = gl.projects.list()
    # print(project)
    variable = project.variables.list()
    print(f"variable: {variable}")
    # variable = project.variables.create({'key': 'IT_STATUS', 'value': False})
    project.variables.update("IT_STATUS", {"value": True})


if __name__ == "__main__":
    main()
