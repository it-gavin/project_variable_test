import os

from pydantic import BaseSettings


class Settings(BaseSettings):
    URL: str
    TOKEN: str
    REPO: int

    class Config:
        env_file = ".env"

def get_settings():
    return Settings()
